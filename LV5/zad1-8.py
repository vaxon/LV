import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as mat
from sklearn.preprocessing import PolynomialFeatures
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing


def generate_data(n):
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    # permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data
#prvi zadatak
np.random.seed(242)
ucenje=generate_data(200) #skup za ucenje
np.random.seed(242)
testiranje=generate_data(100)  #skup za testiranje
plt.figure(0)
#drugi zadatak
plt.scatter(ucenje[:,0],ucenje[:,1], c=ucenje[:,2])

#treci zadatak
logistickimodel = lm.LogisticRegression()
logistickimodel.fit(ucenje[:,0:2], ucenje[:,2])

theta0=logistickimodel.intercept_
theta1=logistickimodel.coef_[0,0]
theta2=logistickimodel.coef_[0,1]

x2=(-theta0-theta1*ucenje[:,0])/theta2
x2=x2[:,np.newaxis]
plt.figure(1)
plt.scatter(ucenje[:,0], ucenje[:,1], c=ucenje[:,2])
plt.plot(ucenje[:,0],x2) 

plt.figure(2)
#cetvrti zadatak
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(ucenje[:,0])-0.5:max(ucenje[:,0])+0.5:.05,min(ucenje[:,1])-0.5:max(ucenje[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = logistickimodel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="winter", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')

#peti zadatak
logistickimodel = lm.LogisticRegression()
logistickimodel.fit(testiranje[:,0:2],testiranje[:,2])
theta3=logistickimodel.intercept_
theta4=logistickimodel.coef_[0,0]
theta5=logistickimodel.coef_[0,1]

testiranje_p=logistickimodel.predict(testiranje[:,0:2])
pogreska=np.subtract(testiranje_p,testiranje[:,2]) 

for i in range(0, 100):
    boja = []
    if pogreska[i]==1:
        boja.append('black')
    else:
        boja.append('green')

plt.scatter(testiranje[:,0], testiranje[:,1], c=pogreska)

x3=(-theta3-theta4*testiranje[:,0])/theta5
x3=x3[:,np.newaxis]
plt.figure(3)
plt.plot(testiranje[:,0],x3)

#sesti zadatak
def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
        
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    
    width = len(c_matrix)
    height = len(c_matrix[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
            
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

matrica_zabune=mat.confusion_matrix(testiranje[:,2],testiranje_p)
plot_confusion_matrix(matrica_zabune)
##sedmi zadatak
#poly = PolynomialFeatures(degree=3, include_bias = False)
#data_train_new = poly.fit_transform(ucenje[:,0:2])
##treci unutar sedmog
#logistickimodel2 = lm.LogisticRegression()
#logistickimodel2.fit(data_train_new[:,0:2], data_train_new[:,2])
#
#theta00=logistickimodel2.intercept_
#theta11=logistickimodel2.coef_[0,0]
#theta22=logistickimodel2.coef_[0,1]
#
#x22=(-theta00-theta11*data_train_new[:,0])/theta22
#x22=x22[:,np.newaxis]
#plt.figure()
#plt.scatter(data_train_new[:,0], data_train_new[:,1], c=data_train_new[:,2])
#plt.plot(data_train_new[:,0],x2) 
#osmi zadatak
def plot_KNN(KNN_model, X, y):
    x1_min, x1_max = X[:, 0].min() -1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() -1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
             np.arange(x2_min, x2_max, 0.01))
    Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z1.reshape(xx.shape)
    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
    plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu', edgecolor='white', label = 'train')