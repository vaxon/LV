brojac = 0;
zbroj = 0;
min=0;
max=0;
while True:    # beskonacna petlja
    try:
        n = raw_input("Upisite broj, kada ste gotovi s upisivanjem, upisite Done: ")
        if n == "Done": #izlazi iz petlje kad korisnik upise Done
            break 
        else:
            broj = float(n) #uneseni n se pridruzuje kao float varijabli broj
            if broj > max: #trazenje maximuma
                max=broj
            if brojac == 0: #postavljanje prvog broja kao minimum
                min=broj
            if broj < min: #trazenje minimuma
                min=broj
        zbroj = zbroj+broj #dodavanje u varijablu zbroj
        brojac=brojac+1 #brojac koraka
    except ValueError: #ispis u slucaju neispravnog unosa i vracanje u beskonacnu petlju
        print("Neispravan unos")
prosjek=zbroj/brojac #racunanje prosjeka
print ("Maksimum vasih brojeva je "),max #ispis maximuma
print ("Minimum vasih brojeva je "), min #ispis minimuma
print ("Prosjek vasih brojeva je: "),prosjek #ispis prosjeka