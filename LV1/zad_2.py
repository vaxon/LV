upis=raw_input("Upisite broj izmedu 0 i 1: ") #unos broja
try:
        ocjena=float(upis)
        if ocjena >1 or ocjena <0: #uvjet da broj bude unutar 0 i 1
            print("Unijeli ste broj izvan dosega:")
        elif ocjena >=0.9: #uvjet za A
            print("A")
        elif ocjena >=0.8: #uvjet za B
            print("B")
        elif ocjena>=0.7: #uvjet za C
            print ("C")
        elif ocjena>=0.6: #uvjet za D
            print ("D")
        elif ocjena<0.6: #uvjet za F
            print("F")  
except ValueError:
        print ("Nije unesen broj!!!") #u slucaju ne unosenja broja