import numpy as np 
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import scale
import matplotlib.pyplot as plt
 
def add_noise(y): 
 
    np.random.seed(14)     
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy 
 
 
def non_func(n): 
    x = np.linspace(1,10,n)
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    y_measured = add_noise(y)
    data = np.concatenate((x,y,y_measured),axis = 0)
    data = data.reshape(3,n)
    return data.T
    
def plotDecisionCurve(x,y):
    h=.02
    # create a mesh to plot in
    x_min, x_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    y_min, y_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    coeff=np.c_[xx.ravel(), yy.ravel()]
    # testiranje svakog dijela kreiranog mesha
    fig, ax = plt.subplots(figsize=(8, 6))
    Z = ANN.predict(coeff)
    
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    ax.contourf(xx, yy, Z, cmap=plt.cm.Paired)
    ax.axis('off')
    
    # Plot also the training points
    ax.scatter(x[:, 0], x[:, 1], c=y, cmap=plt.cm.Paired)
    
    ax.set_title("MultiLayered Perceptron Regresor")
    
data=np.random.permutation(scale(non_func(1000)))
train=data[0:int(0.7*len(data)),:]
test=data[int(0.7*len(data)):,:]

ANN=MLPRegressor(hidden_layer_sizes=(3,10),max_iter=1000, activation='logistic')
ANN.fit(train[:,0:2],train[:,2])
y_predict=ANN.predict(test[:,0:2])
plt.plot(test[:,2],y_predict,'b.')
plotDecisionCurve(test[:,0:2],y_predict)
print "R^2 = ", ANN.score(test[:,0:2],y_predict)
