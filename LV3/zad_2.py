import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('mtcars.csv') #ucitavanje mtcars.csv

#Pomoću barplota prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
v_4=(df[df.cyl == 4]).mpg 
v_6=(df[df.cyl == 6]).mpg
v_8=(df[df.cyl == 8]).mpg
graf=pd.DataFrame({'8-cilindar': v_8, '6-cilindar': v_6 , '4-cilindar':v_4}, columns=['8-cilindar', '6-cilindar','4-cilindar'])
graf.plot(kind='bar')
plt.xlabel('Redni broj auta')
plt.ylabel('Miles per gallon')

#2.Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
wtv4=(df[df.cyl == 4]).wt
wtv6=(df[df.cyl == 6]).wt
wtv8=(df[df.cyl == 8]).wt
graf2=pd.DataFrame({'8-cilindar': wtv8, '6-cilindar': wtv6 , '4-cilindar':wtv4}, columns=['8-cilindar', '6-cilindar','4-cilindar'])
graf2.plot(kind='box')
plt.xlabel('Redni broj auta')
plt.ylabel('Tezina auta')


