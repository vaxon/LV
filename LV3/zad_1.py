import pandas as pd

df = pd.read_csv('mtcars.csv') #ucitavanje mtcars.csv
max_potrosnja = df.sort(['mpg']) #Najveca potrosnja
print max_potrosnja[:-6:-1] #Zadnjih 5 iz liste

v_8=df[(df.cyl == 8)] #trazenje auta s 8 cilindara
v_8_sorted=v_8.sort(['mpg']) #sortiranje po potrosnji
print v_8_sorted.tail(3) #ispis onih s najmanjom potrosnjom

v_6=df[(df.cyl == 6)]
print v_6
prosjek_v_6 = sum(v_6.mpg) / len(v_6.mpg) #stackoverflow.com/questions/15389290
print 'Prosjek v_6 je:', prosjek_v_6

v_4=df[(df.cyl == 4)]
v_4_picked = v_4[(v_4.wt < 2.2) & (v_4.wt > 2)] #uvjet za trazeni interval
prosjek_v_4 = sum(v_4.mpg) / len(v_4.mpg) #racunanje prosjeka
print v_4_picked
print 'Prosjek v_4 između 2000 lbs i 2200 lbs:', prosjek_v_4

automatski=len(df[df.am==1]) #broj auta s automatskim
print 'Broj automatskih:', automatski
rucni=len(df[df.am==0]) #broj auta s rucnim
print 'Broj rucnih', rucni


automatski = df[df.am==1] #polje auta s automatskim
automatski100 = automatski[automatski.hp > 100] #odabir iz polja sa odgovarajucim hp
print 'Automatski mijenjac i preko 100 hp-a:', len(automatski100)

kilogrami=df.wt*0.45*1000 #pounds > kilograms
print "Auti u kilogramima:",kilogrami