import numpy as np
import matplotlib.pyplot as plt

std_dev=np.ones(1000) #inicijalizacija polja
prosjek=np.ones(1000)
for x in range(0, 1000):
    kockice = np.random.choice([1,2,3,4,5,6],30)
    prosjecni=sum(kockice)/30 #racunanje prosjecne vrijednosti kockica
    stand=np.std(kockice) #racunanje standardne devijacije
    std_dev[x]=stand #pridruzivanje odgovarajuce devijacije na odgovarajuce mjesto u polju
    prosjek[x]=prosjecni #pridruzivanje odgovarajuceg prosjeka na odgovarajuce mjesto u polje
plt.hist(prosjek) #crtanje grafova
plt.hist(std_dev)
plt.show()