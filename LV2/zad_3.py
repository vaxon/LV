import numpy as np
import matplotlib.pyplot as plt

kockice = np.random.choice([1,2,3,4,5,6],100)
print kockice
hist, bins = np.histogram(kockice, bins=50)
width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
plt.show()